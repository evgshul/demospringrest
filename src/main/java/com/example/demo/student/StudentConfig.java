package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student mariam = new Student(
                    "Mariam",
                    "mariam.surmane@gmail.com",
                    LocalDate.of(2010, Month.DECEMBER, 17)
            );

            Student misha = new Student(
                    "Mihail",
                    "mihailSh@gmail.com",
                    LocalDate.of(2012, Month.DECEMBER,18)
            );
            repository.saveAll(
                    List.of(mariam, misha)
            );
        };
    }

}
